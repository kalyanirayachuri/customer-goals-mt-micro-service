package customergoalsmicroservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CustomerGoalsMicroServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(CustomerGoalsMicroServiceApplication.class, args);
	}

}

